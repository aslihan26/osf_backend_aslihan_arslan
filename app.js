var fs = require('fs');
var express = require('express');
var path = require('path');
var app = express();
var ejsLayouts =require('express-ejs-layouts');
var db =require('./app_server/models/db');
var bodyParser = require('body-parser')


app.set('view engine', 'ejs'); //EJS
app.set('views', path.join(__dirname, '/app_server/views')); //EJS

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());

app.use(ejsLayouts);
app.use('/public', express.static(path.join(__dirname, '/public'))); //public/css içindeki style.css e ulaşmak için.

//yönlendiriciler ekleniyor
require('./app_server/routes/routeManager')(app);

var User = require('./app_server/models/user');

app.listen(8000);
