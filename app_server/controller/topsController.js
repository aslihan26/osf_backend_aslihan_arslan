//module.exports.index = function(req, res){
//    res.render('denim');
//}
module.exports.index = function(req,res){
    
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  db.collection("products").find({primary_category_id:"womens-clothing-tops"}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    res.render("tops", {tops:result});
    db.close();
  });
});
}