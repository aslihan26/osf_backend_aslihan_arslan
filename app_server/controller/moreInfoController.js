module.exports.index = function(req,res){
    
        var MongoClient = require('mongodb').MongoClient;
        var url = "mongodb://localhost:27017/mydb";
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }

            db.collection("products").findOne({id:req.params.id},function(err,result){
                if (err){
                    throw err;
                }
                res.render("moreInfo",{moreInfo:result});
                console.log(req.params.id)
                console.log(result);
                db.close();
            });
    
        });
}