module.exports.index = function(req,res){
    
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  db.collection("products").find({primary_category_id:"mens-accessories-luggage"}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    res.render("luggage", {luggage:result});
    db.close();
  });
});
}