//module.exports.index = function(req, res){
//    res.render('womens');
//}

module.exports.index = function(req,res){
    
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var query = { categories: "womens" };
  db.collection("categories").find({id:"womens"}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    res.render("womens", {womens:result});
    db.close();
  });
});
}