var express = require('express');
var router = express.Router();
var ctrlscarves = require('../controller/scarvesController');
router.get('/', ctrlscarves.index);
module.exports = router;
