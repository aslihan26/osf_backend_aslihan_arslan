var express = require('express');
var router = express.Router();
var ctrlshoes = require('../controller/shoesController');
router.get('/', ctrlshoes.index);
module.exports = router;
