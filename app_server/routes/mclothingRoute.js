var express = require('express');
var router = express.Router();
var ctrlmCloth = require('../controller/mclothingController');
router.get('/', ctrlmCloth.index);
module.exports = router;
