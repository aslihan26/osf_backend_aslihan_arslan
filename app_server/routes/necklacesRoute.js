var express = require('express');
var router = express.Router();
var ctrlnec = require('../controller/necklacesController');
router.get('/', ctrlnec.index);
module.exports = router;
