var express = require('express');
var router = express.Router();
var ctrlsuits = require('../controller/suitsController');
router.get('/', ctrlsuits.index);
module.exports = router;
