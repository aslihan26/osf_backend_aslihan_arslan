var express = require('express');
var router = express.Router();
var ctrlluggage = require('../controller/luggageController');
router.get('/', ctrlluggage.index);
module.exports = router;
