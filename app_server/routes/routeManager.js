
var routeLogin = require('./loginRoutes');
var routeHome = require('./homeRoutes');
var routeSignUp = require('./signUpRoutes');
var menRoute = require('./menRoute');
var womenRoute = require('./womenRoute');
var clothingRoutes = require('./clothingRoutes');
var outfitsRoute = require('./outfitsRoute');
var topsRoute = require('./topsRoute');
var dressesRoute = require('./dressesRoute');
var bottomsRoute = require('./bottomsRoute');
var jacketsRoute = require('./jacketsRoute');
var jewelryRoute = require('./jewelryRoute');
var accessoriesRoute = require('./accessoriesRoute');
var earringsRoute =require('./earringsRoute');
var braceletsRoute =require('./braceletsRoute');
var necklacesRoute = require('./necklacesRoute');
var mclothingRoute = require('./mclothingRoute');
var suitsRoute =require('./suitsRoute');
var mjacketsRoute = require('./mjacketsRoute');
var mdressRoute = require('./mdressRoute');
var shortRoute = require('./shortRoute');
var pantsRoute = require('./pantsRoute');
var mAccessoriesRoute = require('./mAccessoriesRoute');
var tiesRoute = require('./tiesRoute');
var glovesRoute = require('./glovesRoute');
var luggageRoute = require('./luggageRoute');
var accessoriesRoute = require('./accessoriesRoute');
var scarvesRoute =  require('./scarvesRoute');
var shoesRoute = require('./shoesRoute');
var moreInfoRoute = require('./moreInfoRoute');


//var wMoreInfoRoute = require('./wMoreInfoRoute')
//var feelingRedRoute = reqiure('./feelingRedRoute'); //It is empty.

module.exports = function(app){
    app.use('/login', routeLogin);
    app.use('/signUp', routeSignUp);
    app.use('/', routeHome);
    app.use('/mens', menRoute);
    app.use('/mens/clothing', mclothingRoute);
    app.use('/mens/clothing/mensclothing/suits', suitsRoute);
    app.use('/mens/clothing/mensclothing/Jackets%20&%20Coats', mjacketsRoute);
    app.use('/mens/clothing/mensclothing/Dress%20Shirts', mdressRoute);
    app.use('/Mens/clothing/mensclothing/pants', pantsRoute);
    app.use('/Mens/clothing/mensclothing/shorts', shortRoute);
    app.use('/mens/accessories', mAccessoriesRoute);
    app.use('/Mens/accessories/mAccessories/ties', tiesRoute);
    app.use('/Mens/accessories/mAccessories/gloves', glovesRoute);
    app.use('/Mens/accessories/mAccessories/luggage',luggageRoute);
    app.use('/mens/accessories/mensclothing/suits/:id', moreInfoRoute);


    
    app.use('/womens', womenRoute);
    app.use('/womens/clothing', clothingRoutes);
    app.use('/womens/clothing/outfits', outfitsRoute);
    app.use('/womens/clothing/tops', topsRoute);
    app.use('/womens/clothing/dresses', dressesRoute);
    app.use('/womens/clothing/bottoms', bottomsRoute);
    app.use('/womens/clothing/jackets%20&%20Coats', jacketsRoute);
   // app.use('/womens/clothing/Feeling%20Red', feelingRedRoute);// It is empty
    
    //app.use('/womens/clothing/outfits/:id', wMoreInfoRoute);
    app.use('/womens/Jewelry', jewelryRoute);
    app.use('/womens/jewelry/earrings', earringsRoute);
    app.use('/womens/jewelry/bracelets',braceletsRoute);
    app.use('/womens/jewelry/necklaces',necklacesRoute);
    app.use('/womens/accessories', accessoriesRoute);
    app.use('/womens/accessories/scarves', scarvesRoute);
    app.use('/womens/accessories/shoes', shoesRoute);
    

    
}