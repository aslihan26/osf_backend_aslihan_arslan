var express = require('express');
var router = express.Router();
var ctrlCloth = require('../controller/clothingController');
router.get('/', ctrlCloth.index);
module.exports = router;
