var express = require('express');
var router = express.Router();
var ctrlWoMen = require('../controller/womensController');
router.get('/', ctrlWoMen.index);
module.exports = router;
