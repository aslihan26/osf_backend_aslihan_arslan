var express = require('express');
var router = express.Router();
var ctrltops = require('../controller/topsController');
router.get('/', ctrltops.index);
module.exports = router;
