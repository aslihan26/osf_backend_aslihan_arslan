var express = require('express');
var router = express.Router();
var ctrlred = require('../controller/feelingRedController');
router.get('/', ctrlred.index);
module.exports = router;
