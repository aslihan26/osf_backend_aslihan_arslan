var express = require('express');
var router = express.Router();
var ctrlAccess = require('../controller/accessoriesController');
router.get('/', ctrlAccess.index);
module.exports = router;
