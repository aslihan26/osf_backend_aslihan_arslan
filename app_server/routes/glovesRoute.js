var express = require('express');
var router = express.Router();
var ctrlgloves = require('../controller/glovesController');
router.get('/', ctrlgloves.index);
module.exports = router;
