var express = require('express');
var router = express.Router();
var ctrlJewelry = require('../controller/jewelryController');
router.get('/', ctrlJewelry.index);
module.exports = router;
