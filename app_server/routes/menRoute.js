var express = require('express');
var router = express.Router();
var ctrlMen = require('../controller/mensController');
router.get('/', ctrlMen.index);
module.exports = router;