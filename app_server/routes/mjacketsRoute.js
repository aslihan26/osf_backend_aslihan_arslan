var express = require('express');
var router = express.Router();
var ctrlmjackets = require('../controller/mjacketsController');
router.get('/', ctrlmjackets.index);
module.exports = router;
