var express = require('express');
var router = express.Router();
var ctrlSignUp = require('../controller/signUpController');
router.get('/', ctrlSignUp.index);
router.get('/signUp', ctrlSignUp.loginGet);
module.exports = router;

