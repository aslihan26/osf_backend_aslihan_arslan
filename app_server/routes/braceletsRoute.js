var express = require('express');
var router = express.Router();
var ctrlbra = require('../controller/braceletsController');
router.get('/', ctrlbra.index);
module.exports = router;
