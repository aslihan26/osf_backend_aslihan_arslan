

var express = require('express');
var router = express.Router();
var ctrlLogin = require('../controller/loginController');
router.get('/', ctrlLogin.indexGet);
router.post('/', ctrlLogin.indexPost);

router.get('/signUp', ctrlLogin.signUpGet);
router.post('/signUp', ctrlLogin.signUpPost);

module.exports = router;