var express = require('express');
var router = express.Router();
var ctrlbottoms = require('../controller/bottomsController');
router.get('/', ctrlbottoms.index);
module.exports = router;
