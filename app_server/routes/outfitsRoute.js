var express = require('express');
var router = express.Router();
var ctrlOutfits = require('../controller/outfitsController');
router.get('/', ctrlOutfits.index);
module.exports = router;
