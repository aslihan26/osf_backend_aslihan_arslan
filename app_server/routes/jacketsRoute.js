var express = require('express');
var router = express.Router();
var ctrljackets = require('../controller/jacketsController');
router.get('/', ctrljackets.index);
module.exports = router;
